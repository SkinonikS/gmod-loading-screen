<?php

/**
 * Disable error reporting.
 */
error_reporting(0);
ini_set('display_errors', 'Off');

/**
 * Configuration file.
 */
$config = require(__DIR__ . '/config.php');

/**
 * Default values.
 */
$profile = [
    'name'    => 'Unknown',
    'steamid' => 'STEAM_X:Y:Z',
];

/**
 * Fetch player information if SteamID exists in the query.
 */
if (isset($_GET['steamid'])) {

    /**
     * Fetch player information using SteamAPI.
     */
    $profile = new SimpleXMLElement(sprintf('https://steamcommunity.com/profiles/%s/?xml=1', $_GET['steamid']), null, true);
    $profile['name'] = $profile->xpath('/profile/steamID')[0];

    /**
     * Convert SteamID64 to SteamID2.
     */
    $authserver = bcsub($_GET['steamid'], '76561197960265728') & 1;
    $authid = (bcsub($_GET['steamid'], '76561197960265728') - $authserver) / 2;
    $profile['steamid'] = "STEAM_0:{$authserver}:{$authid}";
}

/**
 * Determine if background is a url.
 */
if ((stristr($config['background'], '#') || stristr($config['background'], 'rgb(') || stristr($config['background'], 'rgba(')) === false) {
    $config['background'] = sprintf('url(\'%s\')', $config['background']);
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>GMOD Loading Screen</title>
    <meta name=“robots” content=“noindex,nofollow”>
    <meta name="language" content="en">
    <meta name="charset" content="utf-8">
    <link type="text/css" rel="stylesheet" href="css/animate.min.css" media="screen,projection">
    <link type="text/css" rel="stylesheet" href="css/app.css" media="screen,projection">
</head>

<body style="background: <?php print $config['background']; ?>; color: <?php print (string) $config['fontColor']; ?>" lang='en'>
    <!-- Page Wrapper -->
    <div class="page-wrapper">
        <!-- Logo Wrapper -->
        <div class="logo-wrapper">
            <div class="animated zoomInDown">
                <img src="<?php print (string) $config['logo']; ?>">
            </div>
        </div>
        <!-- Card Wrapper -->
        <div class="card-wrapper">
            <!-- Server and player information -->
            <div class="card-panel animated zoomInLeft" style="background-color: <?php print (string) $config['themeColor']; ?>;">
                <table class="table-right">
                    <colgroup>
                        <col width="9%">
                        <col width="20%">
                        <col width="71%">
                    </colgroup>
                    <tbody>
                        <tr class="table-title" style="border-color: <?php print (string) $config['fontColor']; ?>;">
                            <td colspan="3">Server Info</td>
                        </tr>
                        <tr>
                            <td>
                                <i class="fas fa-home icon"></i>
                            </td>
                            <td>Name:</td>
                            <td id="server_name">Connecting...</td>
                        </tr>
                        <tr>
                            <td>
                                <i class="fas fa-globe icon"></i>
                            </td>
                            <td>Map:</td>
                            <td id="server_map">RP_xxx</td>
                        </tr>
                        <tr>
                            <td>
                                <i class="fas fa-users icon"></i>
                            </td>
                            <td>Slots:</td>
                            <td id="server_slots">50</td>
                        </tr>
                        <tr>
                            <td>
                                <i class="fas fa-code icon"></i>
                            </td>
                            <td>Mode:</td>
                            <td id="server_gamemode">DarkRP</td>
                        </tr>
                        <tr class="table-title" style="border-top: 1px solid <?php print (string) $config['fontColor']; ?>; border-color: <?php print (string) $config['fontColor']; ?>;">
                            <td colspan="3">Player Info</td>
                        </tr>
                        <tr>
                            <td>
                                <i class="fas fa-user icon"></i>
                            </td>
                            <td>Name:</td>
                            <td><?php print $profile['name']; ?></td>
                        </tr>
                        <tr>
                            <td>
                                <i class="fab fa-steam-symbol icon"></i>
                            </td>
                            <td>SteamID:</td>
                            <td><?php print $profile['steamid']; ?></td>
                        </tr>
                        <tr>
                            <td>
                                <i class="fas fa-download icon"></i>
                            </td>
                            <td>File:</td>
                            <td id="downloading_file">No File Downloading...</td>
                        </tr>
                        <tr>
                            <td>
                                <i class="fas fa-sync-alt icon"></i>
                            </td>
                            <td>Status:</td>
                            <td id="client_status">Retrieving server info...</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- Rules -->
            <div class="card-panel animated zoomInRight" style="background-color: <?php print (string) $config['themeColor']; ?>">
                <table class="table-left" <?php if (count($config['rules']) < 10) : ?>style="height: auto; !important;" <?php endif; ?>>
                    <colgroup>
                        <col width="9%">
                        <col width="91%">
                    </colgroup>
                    <tbody>
                        <tr class="table-title" style="border-color: <?php print (string) $config['fontColor']; ?>;">
                            <td colspan="2">Rules</td>
                        </tr>
                        <?php $count = count($config['rules']); ?>
                        <?php for ($i = 0; $i < $count; $i++) : ?>
                            <?php if ($i >= 10) break; ?>
                            <tr>
                                <td><?php print sprintf('%02d.', $i + 1); ?></td>
                                <td><?php print $config['rules'][$i]; ?></td>
                            </tr>
                        <?php endfor; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- Progress Wrapper -->
        <div class="progress-wrapper">
            <div class="progress animated zoomInUp" style="background-color: <?php print (string) $config['themeColor']; ?>">
                <div id="progress_percent" class="determinate" style="width: 0; background-color: <?php print (string) $config['determinateColor']; ?>"></div>
                <div id="progress_percent_text">0%</div>
            </div>
        </div>
    </div> <!-- Javascript code -->
    <script src="/js/howler.min.js"></script>
    <script src="/js/fontawesome.min.js"></script>
    <script src="/js/app.js"></script>
    <script>
        /**
         * 
         */
        const audioPlayer = new AudioPlayer(JSON.parse('<?php print json_encode((array) $config['musicPlaylist']); ?>'));

        audioPlayer.play();

        /**
         * 
         */
        const loadingScreen = new LoadingScreen(<?php print json_encode((array) $config['gamemodesNames']); ?>);

        /**
         * @param {String} file
         */
        window.SetStatusChanged = function(status) {
            loadingScreen.onStatusChanged(status);
        }

        /**
         * @param {Number} file
         */
        window.SetFilesNeeded = function(status) {
            loadingScreen.setNeededFiles(status);
        }

        /**
         * @param {Number} file
         */
        window.SetFilesTotal = function(status) {
            loadingScreen.setTotalFiles(status);
        }

        /**
         * @param {String} file
         */
        window.DownloadingFile = function(file) {
            loadingScreen.onFileDownloading(file);
        }

        /**
         * @param {String} serverName
         * @param {String} serverUrl
         * @param {String} mapName
         * @param {Number} maxPlayers
         * @param {String} steamId
         * @param {String} gamemodeName
         */
        window.GameDetails = function(serverName, serverUrl, mapName, maxPlayers, steamId, gamemodeName) {
            loadingScreen.setServerInfo(serverName, mapName, maxPlayers, gamemodeName);
        };
    </script>
</body>

</html>
