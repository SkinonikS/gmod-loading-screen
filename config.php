<?php

return [
    /**
     * Color of the font.
     * More information about HTML colors here: https://en.wikipedia.org/wiki/Web_colors.
     *
     * @var string
     */
    'fontColor' => '#fff',

    /**
     * Color of the panels and the progress bar.
     * More information about HTML colors here: https://en.wikipedia.org/wiki/Web_colors.
     *
     * @var string
     */
    'themeColor' => 'rgba(0, 0, 0, 0.3)',

    /**
     * Color of the progress bar determinant.
     * More information about HTML colors here: https://en.wikipedia.org/wiki/Web_colors.
     *
     * @var string
     */
    'determinateColor' => 'rgb(154, 44, 31)',

    /**
     * A color, URI path, orfull URL to the loading screen background.
     * More information about HTML colors here: https://en.wikipedia.org/wiki/Web_colors.
     *
     * @var string
     */
    'background' => 'rgba(0, 0, 0, 0.1)',

    /**
     * A URI path or full URL to the loading screen logo.
     * The recommended size of logo is 800x156 pixels.
     *
     * @var string
     */
    'logo' => '',

    /**
     * Playlist of music.
     * Music must be in OGG format.
     * 
     * @var array
     */
    'musicPlaylist' => [
        // [
        //     /**
        //      * Path to the OGG file.
        //      */
        //     'src' => 'path/to/the/OGG/file',

        //     /**
        //      * Music volume. (min=0.0, max=1.0).
        //      */
        //     'volume' => 0.5,
        // ],
    ],

    /**
     * Server rules. (One rule per row.)
     * If you paste more then 10 rules, they will be ignored.
     *
     * @var array
     */
    'rules' => [
        'Respect staff and other players!',
        'Don\'t kill players without a reason (RDM)',
        'Don\'t kill players with cars (CDM)',
        'Don\'t abuse your job in any way or form!',
        'Don\'t break new life rule (NLR)',
        'Don\'t mic spam or spam in chat.',
        'Don\'t propkill, proppush, propsurf.',
        'Don\'t spawnkill!',
        'Don\'t troll, shit talk or anything like that.',
        'NLR Time - 5 Min.',
    ],

    /**
     * Gamemode names.
     * 
     * @var array
     */
    'gamemodesNames' => [
        'darkrp' => 'DarkRP',
        'terrortown' => 'Trouble in Terrorist Town',
        //'gamemode_code' => 'Gamemode Full Name'
    ],
];
